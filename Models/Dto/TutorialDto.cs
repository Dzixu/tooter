﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Models.Dto
{
    public class TutorialDto
    {
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }
        [JsonProperty( PropertyName = "elements")]
        public ICollection<ElementDto> Elements { get; set; }
    }

    public class ElementDto
    {
        [JsonProperty( PropertyName = "key")]
        public string Key { get; set; }
        [JsonProperty( PropertyName = "id")]
        public string Id { get; set; }
        [JsonProperty( PropertyName = "elementType")]
        public string ElementType { get; set; }
        [JsonProperty( PropertyName = "type")]
        public string Type { get; set; } 
        [JsonProperty( PropertyName = "data")]
        public dynamic Data { get; set; }
    }
}