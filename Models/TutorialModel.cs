﻿using Framework.Models.Entity;
using Models.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class TutorialModel : Entity
    {
        public ICollection<ElementModel> Elements { get; set; }
    }

    public class ElementModel
    {
        public string Key { get; set; }
        public string Id { get; set; }
        public string ElementType { get; set; }
        public string Type { get; set; }
        public dynamic Data { get; set; }

        public ElementModel(ElementDto dto)
        {
            Key = dto.Key;
            Id = dto.Id;
            ElementType = dto.ElementType;
            Type = dto.Type;
            Data = dto.Data;
        }
    }
}
