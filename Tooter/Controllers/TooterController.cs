﻿using Models;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tooter.ViewModel;
using System.Web.Helpers;
using Newtonsoft.Json.Linq;
using Models.Dto;

namespace Tooter.Controllers
{
    public class TooterController : Controller
    {
        private readonly ITutorialService _service;
        public TooterController(ITutorialService service)
        {
            _service = service;
        }

        public ActionResult Index()
        {
            var viewModel = new TooterViewModel()
            {
                BsonObjestId = new BsonObjectId(ObjectId.GenerateNewId()).ToString()
            };

            var dto = new TutorialDto()
            {
                Id = viewModel.BsonObjestId
            };

            _service.CreateTutorial(dto);

            return View(viewModel);
        }

        [HttpPost]
        public JsonResult UpdateElements(TutorialDto dto)
        {
            var result = new { errors="" };
            
            var serviceResult = _service.UpdateTutorial(dto);

            if (!serviceResult.IsValid)
            {
                result = new { errors = serviceResult.ValidationResults.ToString() };
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}