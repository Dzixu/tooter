﻿using Autofac;
using Autofac.Integration.Mvc;
using MongoRepository;
using Services;
using System.Configuration;
using System.Web.Mvc;

namespace Tooter.App_Start
{
    public static class AutofacBootstraper
    {
        public static void AutofacBootstrap()
        {
            var builder = new ContainerBuilder();

            builder.RegisterControllers(typeof(MvcApplication).Assembly);

            builder.RegisterType<TooterDatabase>().As<ITooterDatabase>()
                .WithParameter("connectionString", ConfigurationManager.ConnectionStrings["mongodb"].ConnectionString);

            builder.RegisterGeneric(typeof(DocumentsRepository<>)).As(typeof(IDocumentsRepository<>)).InstancePerDependency();

            builder.RegisterType<TutorialService>().As<ITutorialService>();

            builder.RegisterModelBinders(typeof(MvcApplication).Assembly);
            builder.RegisterModelBinderProvider();

            builder.RegisterModule<AutofacWebTypesModule>();

            builder.RegisterSource(new ViewRegistrationSource());


            builder.RegisterFilterProvider();

            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
    }
}