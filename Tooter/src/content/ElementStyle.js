export const elementStyle = {
    transition: "all 0.5s",
    WebkitTransition: "width 0.3s"
}

export const optionButtonContainerStyle = {
    position:"relative"
}

export const optionButtonStyle = {
    position: "absolute", 
    display: "inline-block", 
    right: "-20px",
    top: "-20px"
}