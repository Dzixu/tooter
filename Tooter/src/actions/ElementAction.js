import dispatcher from "../dispatcher";

export function updatElementData(element, options){
  dispatcher.dispatch({
    type: "UPDATE_ELEMENT",
    element,
    options
  })
}

export function destroyElement(elementId){
  dispatcher.dispatch({
    type: "DESTROY_ELEMENT",
    elementId
  })
}

export function pushUpElement(elementId){
  dispatcher.dispatch({
    type: "PUSH_UP_ELEMENT",
    elementId
  })
}

export function pullDownElement(elementId){
  dispatcher.dispatch({
    type: "PULL_DOWN_ELEMENT",
    elementId
  })
}

