import dispatcher from "../dispatcher";
import axios from "axios";

export function addNewElement(element){
  dispatcher.dispatch({
    type: "NEW_ELEMENT",
    element
  })
}

export function fetchMainStore(store, id){  
  axios.post("/Tooter/UpdateElements", {
    elements: store.elements,
    id: id
  }).then((response) => {
    dispatcher.dispatch({
      type: "UPDATED_STORE"
    });
  }).catch((error) => {
    console.error(error);
  })
}


