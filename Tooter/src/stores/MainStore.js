import { EventEmitter } from "events";
import * as React from "react";

import dispatcher from "../dispatcher";

class MainStore extends EventEmitter {
    constructor() {
        super()

        this.store = {
            elements: [],
            loading: false
        }

        this.oldStore = {
            elements: this.store.elements.map((value) => {
                return { key: value.elementId, id: value.elementId, type: value.type, data: value.data, elementType: value.elementType, options: value.options}
            }),
            loading: this.store.loading
        }
    }

    addNewElement(element){
        const store = this.store;
        store.elements.push(element);
        this.updateStore(store);
    }

    removeElement(elementId){
        const store = this.store;
        const index = store.elements.indexOf(store.elements.find(x => x.id == elementId));
        store.elements.splice(index, 1);
        this.updateStore(store);
    }

    updateElement(element, options){
        const store = this.store;
        store.loading = true;

        const index = store.elements.indexOf(store.elements.find(x => x.id == element.elementId));
        const newElement = {key: element.elementId, id: element.elementId, type: element.type, data: element.data, elementType: element.elementType, options: options};
        store.elements[index] = newElement;
        this.updateStore(store);
    }

    pushUpElement(elementId){
        const store = this.store;
        const index = store.elements.indexOf(store.elements.find(x => x.id == elementId));
        const toSwitchIndex = index-1;

        store.elements[index].options.order = toSwitchIndex;
        store.elements[toSwitchIndex].options.order = index;

        store.elements.sort((a,b) => {
            return a.options.order - b.options.order;
        });
        console.log(store.elements);

        this.updateStore(store);
    }

    pullDownElement(elementId){
        const store = this.store;
        const index = store.elements.indexOf(store.elements.find(x => x.id == elementId));
        const toSwitchIndex = index+1;

        store.elements[index].options.order = toSwitchIndex;
        store.elements[toSwitchIndex].options.order = index;

        store.elements.sort((a,b) => {
            return a.options.order - b.options.order;
        });
        console.log(store.elements);

        this.updateStore(store);
    }

    fetchingData(){
        const store = this.store;
        store.loading = true;
        this.updateStore(store);
    }

    updatedData(){
        const store = this.store;
        store.loading = false;
        this.updateStore(store);
    }

    getStore() {
        const store = this.store;
        // console.log(JSON.stringify(this.oldStore));
        // console.log(JSON.stringify(store));
        return store;
    }

    updateStore(store){
        this.store = store;
        this.emit("change");
    }

    handleActions(action) {
        switch (action.type) {
            case "NEW_ELEMENT":
            {
                this.addNewElement(action.element);
                break;
            }
            case "DESTROY_ELEMENT":
            {
                this.removeElement(action.elementId);
                break;
            }
            case "UPDATE_ELEMENT":
            {
                this.updateElement(action.element, action.options);
                break;
            }
            case "PUSH_UP_ELEMENT":
            {
                this.pushUpElement(action.elementId);
                break;
            }
            case "PULL_DOWN_ELEMENT":
            {
                this.fetchingData();
                break;
            }
            case "UPDATED_STORE":
            {
                this.updatedData();
                break;
            }
        }
    }

}

const mainStore = new MainStore;
dispatcher.register(mainStore.handleActions.bind(mainStore));
window.dispatcher = dispatcher;
export default mainStore;