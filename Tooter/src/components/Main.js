import * as React from "react";

import {Element} from "./Main/Element";

import * as Utils from "../utils/Utils";

import MainStore from "../stores/MainStore";
import * as MainAction from "../actions/MainAction";

export class Main extends React.Component {

  constructor() {
    super();
    this.getData = this.getData.bind(this);
    this.tutorialId = window.tutorialId;
    this.store = MainStore.getStore();
    this.oldStore = {
            elements: this.store.elements.map((value) => {
                return { key: value.elementId, id: value.elementId, type: value.type, data: value.data, elementType: value.elementType, options: value.options}
            }),
            loading: this.store.loading
        }

    const data = {
      title: "title",
      subtitle: "subtitle"
    };
    this.store.elements.push({key: "title", id: "title", data: data, elementType: "TITLE", options: { canRemove: false, order: 0 }})

    this.state = {
      store: this.store
    }
  }

  componentWillMount(){
    MainStore.on("change", this.getData);
  }

  getData(){
    const store = MainStore.getStore();
    const id = this.tutorialId;
    this.setState({store})

    if( JSON.stringify(store) !== JSON.stringify(this.oldStore)){
      this.oldStore = {
          elements: store.elements.map((value) => {
              return { key: value.id, id: value.id, type: value.type, data: value.data, elementType: value.elementType, options: value.options}
          }),
          loading: store.loading
      }
      MainAction.fetchMainStore(store, id);
    }
  }

  addElement(){
    const id = Utils.uuid();
    let elementsCount = this.store.elements.length;
    const elementType = "SECTION"; 
    let element = {key: id, id: id, data: {}, elementType: "SECTION", options: { canRemove: true, order:  elementsCount }};
    MainAction.addNewElement(element);
  }

  render() {
    const el = this.state.store.elements;
    const elements = this.state.store.elements.map((value, index) =>{
      return <Element key={value.id} elementType={value.elementType} id={value.id} data={value.data} options={value.options} allElements={el.length}/>
    });
    const loading = this.state.store.loading;
    let loadingElement = <p class="valing">Zapisywanie...</p>;
    if(!loading){
      loadingElement = [];
    }

    return (
      <div class="container">
        <div class="valign-wrapper">
          <h1 class="valing"><i class="medium material-icons">dashboard</i> Tooter - <small>generator tutoriali</small></h1>
        </div>
        <div class="valign-wrapper">
          {loadingElement}
        </div>
        <div class="row">
          {elements}
        </div>
        <div class="row">
          <div class="col s12 center-align">
            <a class="btn-floating btn-large waves-light green" onClick={this.addElement.bind(this)}><i class="material-icons">add</i></a>
          </div> 
        </div>
      </div>
    )
  }
}