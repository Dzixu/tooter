import React from "react";

import {Title} from "../Title";

export class TitlePreview extends React.Component{
    constructor(props){
        super(props);
    }

    titleEditToggle(){
        this.props.editMode();
    }

    render(){
        const title = this.props.title;

        return (<h2 onClick={this.titleEditToggle.bind(this)}>{title}</h2>)
    }
}