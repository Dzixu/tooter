import React from "react";

import {Title} from "../Title";

export class SubtitlePreview extends React.Component{
    constructor(props){
        super(props);
    }

    subtitleEditToggle(){
        this.props.editMode();
    }

    render(){
        const subtitle = this.props.subtitle;

        return (<div class="flow-text" onClick={this.subtitleEditToggle.bind(this)}>{subtitle}</div>)
    }
}