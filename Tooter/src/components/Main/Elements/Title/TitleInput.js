import React from "react";

import {Title} from "../Title";

export class TitleInput extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            title: this.props.title
        }
    }

    titleEditToggle(e){
        this.props.editMode(e.target.value);
    }
    
    componentDidMount(){
        this.titleInput.focus();
    }

    changeTitle(event){
        const state = this.state;
        state.title = event.target.value;
        this.setState({state});
    }

    render(){
        const title = this.state.title;

        return (<div><input ref={(input) => { this.titleInput = input; }} type="text" onChange={this.changeTitle.bind(this)}
                                onBlur={this.titleEditToggle.bind(this)} value={title} 
                                id="titleInput" placeholder="title"/></div>)
    }
}