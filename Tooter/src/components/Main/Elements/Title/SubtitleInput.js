import React from "react";

import {Title} from "../Title";

export class SubtitleInput extends React.Component{
    constructor(props){
        super(props);

        this.state = {
            subtitle: this.props.subtitle
        }
    }

    subtitleEditToggle(e){
        this.props.editMode(e.target.value);
    }

    changeSubtitle(event){
        const state = this.state;
        state.subtitle = event.target.value;
        this.setState({state});
    }

    componentDidMount(){
        this.subtitleInput.focus();
    }


    render(){
        const subtitle = this.state.subtitle;

        return (<div><input ref={(input) => { this.subtitleInput = input; }} type="text" onChange={this.changeSubtitle.bind(this)} 
                                onBlur={this.subtitleEditToggle.bind(this)} value={subtitle} 
                                id="subitleInput" placeholder="subtitle"/></div>)
    }
}