import React from "react"

export class List extends React.Component{
    constructor(props){
        super(props);

        this.elementId = this.props.elementId;
        this.state = {
            list: [],
            editMode: true
        }

        if(this.props.data.list){
            const state = this.state;
            state.list = this.props.data.list;
            this.setState({state});
        }
    }

    editModeToggle(){
        const state = this.state;
        state.editMode ? state.editMode = false : state.editMode = true;
        this.setState({state});
        if(!state.editMode){
            this.updateData();
        }
    }

    updateData(){
        const list = this.state.list;

        const result = {
            elementId: "",
            elementType: "SECTION",
            type: "LIST",
            data: {list: list}
        }

        this.props.update(result);
    }

    addRow(){
        const state = this.state;
        state.list.push({text: "", id: state.list.length+1});
        this.setState({state});
    }

    updateRow(e){
        const state = this.state;
        state.list.find(x => x.id == e.target.id).text = e.target.value;
        this.setState({state});
    }

    removeRow(e){
        const state = this.state;
        const indexToRemove = state.list.indexOf(state.data.list.find(x => x.id == e.target.id));
        state.data.list.splice(indexToRemove, 1);

        let list = state.list.map((value, index) => {
            return {text: value.text, id: index}
        });

        this.setState({state});
    }

    render(){
        const editMode = this.state.editMode;
        let list = [];

        if(this.state.list[0]){
            if(!editMode){
                list = this.state.list.map((value, index) => {
                    return <li class="collection-item" key={index}>{value.id} - {value.text}</li>
                });
            }
            else{
                list = this.state.list.map((value, index) => {
                    return( 
                    <li class="collection-item input-field" key={index}>
                        <a class="btn-floating red prefix" onClick={this.removeRow.bind(this)}>
                            <i class="material-icons">delete</i>
                        </a><input type="text" onChange={this.updateRow.bind(this)} value={value.text} id={value.id}/>
                    </li>
                    )});
            }
        }

        if(!editMode){
            return(
                <div class="col s12" onClick={this.editModeToggle.bind(this)}> 
                   
                    <ul class="collection">
                        {list}
                    </ul>
                </div>)
        }

        return(
            <div class="col s12">
                <div class="col s12">
                    <a class="btn-floating btn-large blue" onClick={this.editModeToggle.bind(this)}><i class="material-icons">done_all</i></a>
                </div>
                <ul class="collection">
                    {list}
                </ul>
                 <div class="row">
                    <div class="col s12 left-align">
                        <a class="btn-floating waves-light green" onClick={this.addRow.bind(this)}><i class="material-icons">add</i></a>
                    </div> 
                </div>
            </div>
        )
    }
}