import React from "react";

import {Input} from "./PlainText/Input";
import {Preview} from "./PlainText/Prewiew";

export class PlainText extends React.Component{
    constructor(props){
        super(props);

        const plainText = "text";

        this.state = {
            editMode: true
        }
    }

    titleEditToggle(value){
        const state = this.state;
        state.editMode = !state.editMode;

        if(!value){
            state.editMode = true;
            window.Materialize.toast("Podana wartość nie moze być pusta", 4000);
        }
        else{
            const result = {
                elementId: "",
                elementType: "SECTION",
                type: "PLAIN_TEXT",
                data: {text: value}
            }
            this.props.update(result);
        }
        
        this.setState({state}); 
    }


    render(){
        const text = this.props.data.text;

        const editMode = this.state.editMode; 

        const textInput = <Input value={text} editMode={this.titleEditToggle.bind(this)}/>;
        const textPreview = <Preview value={text} editMode={this.titleEditToggle.bind(this)}/>

        if(editMode || !text){
            return (
            <div class="col s12">
                <div class="col s12">
                    {textInput}
                </div>
            </div>)
        }
        else{
            return (
            <div class="col s12">
                <div class="col s12">
                    {textPreview}
                </div>
            </div>)
        }


    }
}