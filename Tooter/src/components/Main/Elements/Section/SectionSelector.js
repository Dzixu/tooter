import React from "react";

export class SectionSelector extends React.Component{
    constructor(props){
        super(props);
    }

    componentDidMount(){ 
        window.jQuery('.material-tooltip').remove();
        window.jQuery('.tooltipped').tooltip({delay: 10});
    }

    selectSection(e){
        window.jQuery('.material-tooltip').remove();    
        window.jQuery('.tooltipped').tooltip({delay: 10});
        const type = e.target.getAttribute('data-type');
        this.props.select(type);
    }

    render(){
        return (
            <div class="s12">
                <div class="row"> 
                    <div class="col s6 offset-s3 center-align">
                        <div class="row"></div>
                        <div class="row">
                            <div class="col s3">
                                <a class="btn-floating btn-large tooltipped blue hoverable" 
                                    onClick={this.selectSection.bind(this)} data-position="top" 
                                    data-tooltip="Nagłówek">
                                    <i class="material-icons" data-type="HEADER">label</i>
                                </a>
                            </div>
                            <div class="col s3">
                                <a class="btn-floating btn-large tooltipped green hoverable" 
                                    onClick={this.selectSection.bind(this)} data-position="top" 
                                    data-tooltip="Tekst">
                                    <i class="material-icons" data-type="PLAIN_TEXT">subject</i>
                                </a>
                            </div>
                            <div class="col s3">
                                <a class="btn-floating btn-large tooltipped yellow darken-1 hoverable" 
                                    onClick={this.selectSection.bind(this)} data-position="top" 
                                    data-tooltip="Zdjęcie">
                                    <i class="material-icons" data-type="PHOTO">picture_in_picture</i>
                                </a>
                            </div>
                            <div class="col s3">
                                <a class="btn-floating btn-large tooltipped red hoverable" 
                                    onClick={this.selectSection.bind(this)} data-position="top" 
                                    data-tooltip="Lista wypunktowana">
                                    <i class="material-icons" data-type="LIST">toc</i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
        )

    }
}