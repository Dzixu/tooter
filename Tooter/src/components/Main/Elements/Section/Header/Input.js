import React from "react";

export class Input extends React.Component{
    constructor(props){
        super(props);

        if(this.props.value)
            this.state = {
                text: this.props.value
            }
        else
            this.state = {
                text: ''
            }
    }

    titleEditToggle(){
        const text = this.state.text;
        this.props.editMode(text);
    }

    componentDidMount(){
        this.input.focus();
    }

    changeInput(event){
        const state = this.state;
        state.text = event.target.value;
        this.setState({state});
    }

    render(){
        const value = this.state.text;

        return (
            <div class="row">
                <div class="row"></div>
                <div class="col s12">
                    <a class="btn-floating btn-large tooltipped blue" onClick={this.titleEditToggle.bind(this)}><i class="material-icons">done_all</i></a>
                </div>
                <div class="input-field col s12">
                    <input class="materialize-textarea" ref={(input) => { this.input = input; }} value={value} onChange={this.changeInput.bind(this)} />
                </div>
            </div>
        )
    }
}