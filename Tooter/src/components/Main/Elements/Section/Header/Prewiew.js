import React from "react";

export class Preview extends React.Component{
    constructor(props){
        super(props);
    }

    titleEditToggle(){
        this.props.editMode();
    }
    
    render(){
        let value = this.props.value;

        if(!value) value="text";

        return (
            <div class="flow-text" onClick={this.titleEditToggle.bind(this)}>
                <div class="col s12">
                    <h4>{value}</h4>
                </div>
            </div>
        )
    }
}