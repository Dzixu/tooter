import React from "react";

import {Input} from "./Header/Input";
import {Preview} from "./Header/Prewiew";

export class Header extends React.Component{
    constructor(props){
        super(props);
        
        this.state = {
            editMode: false
        }
    }

    titleEditToggle(value){
        const state = this.state;
        state.editMode = !state.editMode;

        if(!value){
            state.editMode = true;
            window.Materialize.toast("Podana wartość nie moze być pusta", 4000);
        }
        else{
            const result = {
                elementId: "",
                elementType: "SECTION",
                type: "HEADER",
                data: {text: value}
            }
            this.props.update(result);
        }

        this.setState({state});
    }

    render(){
        const text = this.props.data.text;
        const editMode = this.state.editMode; 

        const textInput = <Input value={text} editMode={this.titleEditToggle.bind(this)}/>;
        const textPreview = <Preview value={text} editMode={this.titleEditToggle.bind(this)}/>;

        if(editMode){
            return (
            <div class="col s12">
                    {textInput}
            </div>)
        }
        else{
            return (
            <div class="col s12">
                    {textPreview}
            </div>)
        }


    }
}