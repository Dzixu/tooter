import React from "react";

export class Preview extends React.Component{
    constructor(props){
        super(props);
    }

    titleEditToggle(){
        this.props.editMode();
    }
    
    render(){
        const value = this.props.value;

        return (
            <div class="row flow-text" onClick={this.titleEditToggle.bind(this)}>
                <div class="row"></div>
                <div class="col s12">
                    <div class="materialize-textarea">{value}</div>
                </div>
            </div>
        )
    }
}