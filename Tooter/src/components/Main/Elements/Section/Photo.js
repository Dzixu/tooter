import React from "react"
import Dropzone from 'react-dropzone'

export class Photo extends React.Component{
    constructor(props){
        super(props);

        this.updateData = this.updateData.bind(this);

        if(this.props.data.file)
            this.state = {
                file: this.props.data.file,
            }
        else
            this.state = {
                file: {},
            }
    }

     onDrop(acceptedFiles, rejectedFiles) {
        if(acceptedFiles[0]){
            const state = this.state;
            state.file = acceptedFiles[0];
            this.setState({state});

            this.updateData();
        }
        else{
             window.Materialize.toast(`Podany plik "${rejectedFiles[0].type}" jest niepoprawny! `, 4000);
        }
    };

    updateData(){
        const file = this.state.file;
        const result = {
                elementId: "",
                elementType: "SECTION",
                type: "PHOTO",
                data: {file: file}
            }
        this.props.update(result);
    }

    render(){
        const img = this.state.file.preview;

        if(img){
            return(
            <div class="col s12">
                <div class="col s12 center-align">
                    <img src={img} style={{width: "100%", height: "100%"}}/>
                </div>
            </div>
        )}

        return(
            <div class="col s12">
                <div class="col s12 center-align">
                    <Dropzone onClick={this.onOpenClick} ref={(node) => { this.dropzone = node; }} onDrop={this.onDrop.bind(this)} accept="image/*" style={{border: "1px solid lightgray",width: "100%", height: "200px"}}>
                        <div class="valign-wrapper valign-wrapper">
                            <h5 class="valign center" style={{width: "100%"}}>Upuść zdjęcie lub kliknij aby wybrać zdjęcie.</h5>
                        </div>
                    </Dropzone>
                </div>
            </div>
        )

    }
}