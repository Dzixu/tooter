import React from "react"

import {SectionSelector} from "./Section/SectionSelector"
import {PlainText} from "./Section/PlainText"
import {Header} from "./Section/Header"
import {Photo} from "./Section/Photo"
import {List} from "./Section/List"

export class Section extends React.Component{
    constructor(props){
        super(props);

        this.state = {
            section: { type: "SECTION_SELECTOR", data: {}}
        }
    }

    updateElement(element){
        this.props.updateHandler(element);
    }

    changeSection(type){
        const data = this.props.data;
        const state= this.state;
        state.section.type = type;

        this.setState({state});
    }

    render(){
        const state = this.state;
        const data = this.props.data;
        let section;

         switch (state.section.type) {
            case "PLAIN_TEXT":
                section = <PlainText data={data} update={this.updateElement.bind(this)}/>
                break;
            case "HEADER":
                section = <Header  data={data} update={this.updateElement.bind(this)} />
                break;
            case "PHOTO":
                section = <Photo  data={data} update={this.updateElement.bind(this)} />
                break;
            case "LIST":
                section = <List  data={data} update={this.updateElement.bind(this)} />
                break;
            default:
            section = <SectionSelector select={this.changeSection.bind(this)} />;
                break;
        }

        return (
        <div>
            <div class="section">
                {section}
            </div>
        </div>
        )

    }
}