import React from "react";

import {TitlePreview} from "./Title/TitlePreview";
import {TitleInput} from "./Title/TitleInput"
import {SubtitlePreview} from "./Title/SubtitlePreview";
import {SubtitleInput} from "./Title/SubtitleInput";

export class Title extends React.Component{
    constructor(props){
        super(props);
        this.elementId = this.props.elementId;

        this.updateData = this.updateDataFromTitle.bind(this);
        this.updateData = this.updateDataFromSubtitle.bind(this);
        this.elementFactory = this.elementFactory.bind(this);

        this.state = {
            titleEdit: false,
            subtitleEdit: false
        }
    }

    titleEditToggle(value){
        const state = this.state;
        state.titleEdit = !state.titleEdit;
        this.setState({state});

        if(state.titleEdit == false){
          this.updateDataFromTitle(value);  
        }
    }

    subtitleEditToggle(value){
        const state = this.state;
        state.subtitleEdit = !state.subtitleEdit;
        this.setState({state});

        if(state.subtitleEdit === false) {
            this.updateDataFromSubtitle(value);
        }
    }


    updateDataFromTitle(value){
        this.props.updateHandler(this.elementFactory("TITLE", value));
    }

     updateDataFromSubtitle(value){
        this.props.updateHandler(this.elementFactory("SUBTITLE", value));
    }

    elementFactory(type, value){
        let result;
        switch (type) {
            case "TITLE":
                result = {
                    elementId: this.elementId,
                    elementType: "TITLE",
                    data: {title: value, subtitle: this.props.data.subtitle}
                } 
                break;
            case "SUBTITLE":
                result = {
                    elementId: this.elementId,
                    elementType: "TITLE",
                    data: {title: this.props.data.title, subtitle: value}
                }
                break;
        }
        return result;
    }

    render(){
        const title = this.props.data.title;
        const subtitle = this.props.data.subtitle;

        const titleEdit = this.state.titleEdit;     
        const subtitleEdit = this.state.subtitleEdit;

        const titlePreview =  <TitlePreview title={title} editMode={this.titleEditToggle.bind(this)}></TitlePreview>
        const titleInput = <TitleInput title={title} editMode={this.titleEditToggle.bind(this)}></TitleInput>

        const subtitlePreview =  <SubtitlePreview subtitle={subtitle} editMode={this.subtitleEditToggle.bind(this)}></SubtitlePreview>
        const subtitleInput = <SubtitleInput subtitle={subtitle} editMode={this.subtitleEditToggle.bind(this)}></SubtitleInput>


        if (!titleEdit && !subtitleEdit  && title && subtitle) {
             return (
                 <div class="col s12">
                    <div class="col s12">
                        {titlePreview}
                    </div>
                    <div class="col s12">
                        {subtitlePreview}
                    </div>
                </div>)
        }
        else if (subtitleEdit && !titleEdit && title || !subtitle){
            return (
                <div class="col s12">
                    <div class="col s12">
                        {titlePreview}
                    </div>
                    <div class="col s12">
                        {subtitleInput}
                    </div>
                </div>)
        }
        else if(!subtitleEdit && titleEdit && subtitle || !title){
             return (
                <div class="col s12">
                    <div class="col s12">
                        {titleInput}
                    </div>
                    <div class="col s12">
                        {subtitlePreview}
                    </div>
                </div>)
        }
        else{
            return (
                <div class="col s12">
                    <div class="col s12">
                        {titleInput}
                    </div>
                    <div class="col s12">
                        {subtitleInput}
                    </div>
                </div>)
        }

    }
}