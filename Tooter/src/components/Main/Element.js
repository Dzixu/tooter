import * as React from "react"

import {Title} from "./Elements/Title"
import {Section} from "./Elements/Section"

import * as Style from "../../content/ElementStyle"

import * as ElementAction from "../../actions/ElementAction"

export class Element extends React.Component {

  constructor(props) {
    super(props);
    this.id = this.props.id;
    this.modalId = "modal"+this.id;
    this.canRemove = this.props.options.canRemove;
    this.order = this.props.options.order;

    this.state ={
        hideClass: "hide"
    }
  }

  showEditButton(){
      const state = this.state;
        state.hideClass = "";
        this.setState({state});
  }

  hideEditButton(){
      const state = this.state;
        state.hideClass = "hide";
        this.setState({state});
  }

  componentDidMount(){
    window.jQuery(".modal").modal();
  }

  updateElement(element){
    const options = {
        canRemove: this.canRemove,
        order: this.order
    }
    element.elementId = this.id;
    
    ElementAction.updatElementData(element, options);
  }

  getUpElement(){
      ElementAction.pushUpElement(this.id);
  }

  getDownElement(){
      ElementAction.pullDownElement(this.id);
  }

  destroy(){
    const id = this.id;
    ElementAction.destroyElement(id);
  }

  render() {
    const elementType = this.props.elementType;
    const canRemove = this.props.options.canRemove;
    const order = this.props.options.order;
    const allElements = this.props.allElements;
    const modalId = this.modalId;
    const hideClass = this.state.hideClass;
    let element;

    const elementStyle = Style.elementStyle;
    const optionButtonStyleContainer = Style.optionButtonContainerStyle;
    const optionButtonStyle = Style.optionButtonStyle;

    const data = this.props.data;

    switch (elementType) {
    case "TITLE":
            element = <Title elementId={this.id} data={data} updateHandler={this.updateElement.bind(this)}/>
            break;
    case "SECTION":
            element = <Section data={data} updateHandler={this.updateElement.bind(this)}/>
            break;
    default:
        console.error("element not found!");
        break;
    }

    if(!canRemove){
        return (
        <div class="col s12">
            <div class="row">
                {element}
            </div>
        </div>
    )}

    let upButton = <li><a class="btn-floating blue" onClick={this.getUpElement.bind(this)}><i class="material-icons" style={{transform: "rotate(270deg)"}}>play_arrow</i></a></li>
    let downButton =<li><a class="btn-floating green" onClick={this.getDownElement.bind(this)}><i class="material-icons" style={{transform: "rotate(90deg)"}}>play_arrow</i></a></li>
    
    if(order==1 && allElements == 2){
        upButton = [];
        downButton = [];
    }
    else if(order == 1){
        upButton = [];
    }
    else if(order == allElements-1){
        downButton = []
    }

    return (
        <div class="col s12 white" onMouseEnter={this.showEditButton.bind(this)} onMouseLeave={this.hideEditButton.bind(this)} style={elementStyle}>
            <div style={optionButtonStyleContainer}>
                <div class={hideClass}>
                    <div class="fixed-action-btn horizontal" style={optionButtonStyle}>
                        <a class="btn-floating btn-large red">
                            <i class="large material-icons">mode_edit</i>
                        </a>
                        <ul>
                            {upButton}
                            {downButton}
                            <li><a class="modal-trigger btn-floating red" href={"#"+modalId}><i class="material-icons">delete</i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div id={modalId} class="modal">
                <div class="modal-content">
                    <h4>Modal Header</h4>
                    <p>A bunch of text</p>
                </div>
                <div class="modal-footer">
                    <a href="#!" class=" modal-action modal-close waves-effect waves-green btn-flat" onClick={this.destroy.bind(this)}>Tak</a>
                    <a href="#!" class=" modal-action modal-close waves-effect waves-green btn-flat">Nie</a>
                </div>
            </div>
            <div class="section">
                {element}
            </div>
        </div>
    )
      

  }
}