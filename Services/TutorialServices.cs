﻿using Framework.Dependency;
using Framework.Services;
using Models;
using Models.Dto;
using MongoRepository;
using System;
using System.Linq;

namespace Services
{
    public interface ITutorialService: IDependency
    {
        ServiceResult CreateTutorial(TutorialDto dto);
        ServiceResult UpdateTutorial(TutorialDto dto);
    }

    public class TutorialService: ITutorialService
    {
        private readonly IDocumentsRepository<TutorialModel> _repository;

        public TutorialService(IDocumentsRepository<TutorialModel> repository)
        {
            _repository = repository;
        }

        public ServiceResult CreateTutorial(TutorialDto dto)
        {
            var result = new ServiceResult();

            var record = new TutorialModel()
            {
                Id = dto.Id,
                ModifiedOn = DateTime.Now
            };

            try
            {
                _repository.Insert(record);
            }
            catch (Exception ex)
            {
                result.AddError("Bład: {0}", ex.Message);
            }

            return result;
        }

        public ServiceResult UpdateTutorial(TutorialDto dto)
        {
            var result = new ServiceResult();

            var record = _repository.Get(dto.Id);

            if (record == null)
            {
                result.AddError("Nie znaleziono elementu");
            }
            else
            {
                record.Elements = dto.Elements.Select(x => new ElementModel(x)).ToArray();

                try
                {
                    _repository.Replace(record);
                }
                catch (Exception ex)
                {
                    result.AddError("Bład: {0}", ex.Message);
                }
            }


            return result;
        }
    }
}
