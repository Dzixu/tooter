﻿using MongoDB.Driver;

namespace MongoRepository
{
    public class TooterDatabase : ITooterDatabase
    {
        public IMongoDatabase Database { get; private set; }

        /// <summary>
        /// Method connect with MongoDB database
        /// </summary>
        /// <param name="connectionString">Connection string</param>
        public TooterDatabase(string connectionString)
        {
            var url = new MongoUrl(connectionString);
            Database = new MongoClient(url).GetDatabase(url.DatabaseName);
        }
    }
}
