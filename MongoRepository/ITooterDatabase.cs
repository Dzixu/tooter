﻿using MongoDB.Driver;

namespace MongoRepository
{
    public interface ITooterDatabase
    {
        IMongoDatabase Database { get; }
    }
}
