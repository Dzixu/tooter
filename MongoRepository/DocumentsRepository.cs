﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using MongoDB.Driver;
using Framework.Models.Entity;
using Framework.Models.Attributes;

namespace MongoRepository
{
    public class DocumentsRepository<T> : IDocumentsRepository<T>
        where T : IEntity
    {
        private TooterDatabase itakaDatabase;

        #region MongoSpecific
        public DocumentsRepository(ITooterDatabase itakaLeadDatabase)
        {
            Collection = itakaLeadDatabase.Database.GetCollection<T>(GetCollectionName());
        }

        public DocumentsRepository(TooterDatabase itakaDatabase) :
            this(itakaDatabase as ITooterDatabase)
        {
            this.itakaDatabase = itakaDatabase;
        }

        public IMongoCollection<T> Collection
        {
            get; private set;
        }

        public FilterDefinitionBuilder<T> Filter
        {
            get
            {
                return Builders<T>.Filter;
            }
        }

        public UpdateDefinitionBuilder<T> Updater
        {
            get
            {
                return Builders<T>.Update;
            }
        }

        public ProjectionDefinitionBuilder<T> Project
        {
            get
            {
                return Builders<T>.Projection;
            }
        }

        private IFindFluent<T, T> Query(Expression<Func<T, bool>> filter)
        {
            return Collection.Find(filter);
        }
        #endregion MongoSpecific

        #region CRUD
        /// <summary>
        /// Method gets document from database by its id
        /// </summary>
        /// <param name="id">Document id</param>
        /// <returns>Document model</returns>
        public virtual T Get(string id)
        {
            return Find(i => i.Id == id).FirstOrDefault();
        }

        public IEnumerable<T> Get(IEnumerable<string> ids)
        {
            return Find(i => ids.Contains(i.Id));
        }

        /// <summary>
        /// Method gets all documents from database
        /// </summary>
        /// <returns>Queryable collection of documents</returns>
        public IQueryable<T> All()
        {
            return Collection.AsQueryable();
        }

        public IQueryable<T> FilterBy(Expression<Func<T, bool>> filter)
        {
            return All().Where(filter);
        }

        public T FindBy(Expression<Func<T, bool>> filter)
        {
            return All().FirstOrDefault(filter);
        }

        /// <summary>
        /// Methods finds all documents that meet the filter
        /// </summary>
        /// <param name="filter">Current filter</param>
        /// <returns>List of documents</returns>
        public virtual IEnumerable<T> Find(Expression<Func<T, bool>> filter)
        {
            return Query(filter).ToEnumerable();
        }

        /// <summary>
        /// Method returns paged list of filtered items
        /// </summary>
        /// <param name="filter">Current filter</param>
        /// <param name="pageIndex">Index of the page</param>
        /// <param name="size">Number of items on the page</param>
        /// <returns>List of items</returns>
        public IEnumerable<T> Find(Expression<Func<T, bool>> filter, int pageIndex, int size)
        {
            return Find(filter, i => i.Id, pageIndex, size);
        }

        /// <summary>
        /// Method returns paged and ordered list of filtered items
        /// </summary>
        /// <param name="filter">Current filter</param>
        /// <param name="order">Field which is sorted</param>
        /// <param name="pageIndex">Index of the page</param>
        /// <param name="size">Number of items on the page</param>
        /// <returns>List of items</returns>
        public IEnumerable<T> Find(Expression<Func<T, bool>> filter, Expression<Func<T, object>> order, int pageIndex, int size)
        {
            return Find(filter, order, pageIndex, size, true);
        }

        /// <summary>
        /// Method returns paged and ordered list of filtered items
        /// </summary>
        /// <param name="filter">Current filter</param>
        /// <param name="order">Field which is sorted</param>
        /// <param name="pageIndex">Index of the page</param>
        /// <param name="size">Number of items on the page</param>
        /// <param name="isDescending">True list is sorted by descending</param>
        /// <returns>List of items</returns>
        public virtual IEnumerable<T> Find(Expression<Func<T, bool>> filter, Expression<Func<T, object>> order, int pageIndex, int size, bool isDescending)
        {
            var query = Query(filter).Skip(pageIndex * size).Limit(size);
            return (isDescending ? query.SortByDescending(order) : query.SortBy(order)).ToEnumerable();
        }

        /// <summary>
        /// Insert one item in database
        /// </summary>
        /// <param name="entity">Inserted item</param>
        public virtual void Insert(T entity)
        {
            Collection.InsertOne(entity);
        }

        /// <summary>
        /// Insert many items in database
        /// </summary>
        /// <param name="entities">Collection of items, which are inserted</param>
        public virtual void Insert(IEnumerable<T> entities)
        {
            Collection.InsertMany(entities);
        }

        /// <summary>
        /// Update one item in database
        /// </summary>
        /// <param name="entity"></param>
        public virtual void Replace(T entity)
        {
            Collection.ReplaceOne(i => i.Id == entity.Id, entity);
        }

        /// <summary>
        /// Update list of items in database
        /// </summary>
        /// <param name="entities">List of </param>
        public void Replace(IEnumerable<T> entities)
        {
            foreach (T entity in entities)
            {
                Replace(entity);
            }
        }

        /// <summary>
        /// Method update the field in the entity
        /// </summary>
        /// <typeparam name="TField">Type of field</typeparam>
        /// <param name="entity">Entity</param>
        /// <param name="field">Points the field in the entity</param>
        /// <param name="value">New value of the field</param>
        /// <returns>True if operation is successfull</returns>
        public bool Update<TField>(T entity, Expression<Func<T, TField>> field, TField value)
        {
            return Update(entity, Updater.Set(field, value));
        }

        /// <summary>
        /// Method update data in database
        /// </summary>
        /// <param name="entity">Entity</param>
        /// <param name="update">Rules defining which fields have to be updated</param>
        /// <returns>True if operation is successfull</returns>
        public virtual bool Update(T entity, UpdateDefinition<T> update)
        {
            return Update(Filter.Eq(i => i.Id, entity.Id), update);
        }

        /// <summary>
        /// Method update the field for selected items in the entity
        /// </summary>
        /// <typeparam name="TField">Type of field</typeparam>
        /// <param name="filter">Filter</param>
        /// <param name="field">Points the field in the entity</param>
        /// <param name="value">New value</param>
        /// <returns>True if operation is successfull</returns>
        public bool Update<TField>(FilterDefinition<T> filter, Expression<Func<T, TField>> field, TField value)
        {
            return Update(filter, Updater.Set(field, value));
        }

        /// <summary>
        /// Method update the field for selected items in the entity
        /// </summary>
        /// <param name="filter">Entity</param>
        /// <param name="update">Rules defining which fields have to be updated</param>
        /// <returns>True if operation is successfull</returns>
        public bool Update(FilterDefinition<T> filter, UpdateDefinition<T> update)
        {
            return Collection.UpdateMany(filter, update.CurrentDate(i => i.ModifiedOn)).IsAcknowledged;
        }

        /// <summary>
        /// Method removes entity from database
        /// </summary>
        /// <param name="entity">Entity</param>
        public void Delete(T entity)
        {
            Delete(entity.Id);
        }

        /// <summary>
        /// Method removes selected item from database
        /// </summary>
        /// <param name="id">Id of selected item</param>
        public virtual void Delete(string id)
        {
            Collection.DeleteOne(i => i.Id == id);
        }

        /// <summary>
        /// Method removes selected items from database
        /// </summary>
        /// <param name="filter">Filter defining selected items</param>
        public void Delete(Expression<Func<T, bool>> filter)
        {
            Collection.DeleteMany(filter);
        }

        public void Archive(string id)
        {
            var entity = Get(id);
            Update(entity, x => x.Deleted, true);
            Update(entity, x => x.DeletedOn, DateTime.Now);
        }

        public void Archive(Expression<Func<T, bool>> filter)
        {
            var entities = FilterBy(filter);
            foreach (var item in entities)
            {
                Update(item, x => x.Deleted, true);
                Update(item, x => x.DeletedOn, DateTime.Now);
            }
        }
        #endregion CRUD

        #region Simplicity
        /// <summary>
        /// Check if collection has any item which meet the filter
        /// </summary>
        /// <param name="filter">Filter</param>
        /// <returns>True if at least one item exists</returns>
        public bool Any(Expression<Func<T, bool>> filter)
        {
            return Collection.AsQueryable<T>().Any(filter);
        }


        #endregion Simplicity

        #region Collection Name
        /// <summary>
        /// Determines the collection name for T and assures it is not empty
        /// </summary>
        /// <typeparam name="T">The type to determine the collection name for.</typeparam>
        /// <returns>Returns the collection name for T.</returns>
        private string GetCollectionName()
        {
            string collectionName;
            collectionName = typeof(T).BaseType.Equals(typeof(object)) ?
                                      GetCollectionNameFromInterface() :
                                      GetCollectionNameFromType();

            if (string.IsNullOrEmpty(collectionName))
            {
                collectionName = typeof(T).Name;
            }
            return collectionName.ToLowerInvariant();
        }

        /// <summary>
        /// Determines the collection name from the specified type.
        /// </summary>
        /// <typeparam name="T">The type to get the collection name from.</typeparam>
        /// <returns>Returns the collection name from the specified type.</returns>
        private string GetCollectionNameFromInterface()
        {
            // Check to see if the object (inherited from Entity) has a CollectionName attribute
            var att = Attribute.GetCustomAttribute(typeof(T), typeof(CollectionNameAttribute));

            return att != null ? ((CollectionNameAttribute)att).Name : typeof(T).Name;
        }

        /// <summary>
        /// Determines the collectionname from the specified type.
        /// </summary>
        /// <param name="entitytype">The type of the entity to get the collectionname from.</param>
        /// <returns>Returns the collectionname from the specified type.</returns>
        private string GetCollectionNameFromType()
        {
            Type entitytype = typeof(T);
            string collectionname;

            // Check to see if the object (inherited from Entity) has a CollectionName attribute
            var att = Attribute.GetCustomAttribute(entitytype, typeof(CollectionNameAttribute));
            if (att != null)
            {
                // It does! Return the value specified by the CollectionName attribute
                collectionname = ((CollectionNameAttribute)att).Name;
            }
            else
            {
                if (typeof(Entity).IsAssignableFrom(entitytype))
                {
                    // No attribute found, get the basetype
                    while (!entitytype.BaseType.Equals(typeof(Entity)))
                    {
                        entitytype = entitytype.BaseType;
                    }
                }
                collectionname = entitytype.Name;
            }

            return collectionname;
        }
        #endregion Collection Name
    }
}
