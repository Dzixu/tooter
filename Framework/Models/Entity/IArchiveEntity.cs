﻿using System;

namespace Framework.Models.Entity
{
    public interface IArchiveEntity
    {
        bool Deleted { get; set; }
        DateTime DeletedOn { get; set; }
    }
}
