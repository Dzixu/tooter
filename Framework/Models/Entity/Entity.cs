﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Runtime.Serialization;

namespace Framework.Models.Entity
{
    [BsonIgnoreExtraElements(Inherited = true)]
    public class Entity : IEntity
    {
        public DateTime CreatedOn => ObjectId.CreationTime.ToLocalTime();

        [BsonId]
        [BsonElement("_id", Order = 0)]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("_t", Order = 1)]
        public virtual string EntityType => GetType().Name;

        [BsonElement("_m", Order = 2)]
        [BsonRepresentation(BsonType.DateTime)]
        public DateTime ModifiedOn { get; set; }

        [BsonRepresentation(BsonType.Boolean)]
        [BsonDefaultValue(false)]
        public bool Deleted { get; set; }
        public DateTime DeletedOn { get; set; }

        public ObjectId ObjectId
        {
            get
            {
                //Incase, this is required before inserted into db
                if (Id == null)
                    Id = ObjectId.GenerateNewId().ToString();
                return ObjectId.Parse(Id);
            }
        }

        public static string GenerateNewId()
        {
            return ObjectId.GenerateNewId().ToString();
        }
    }
}
