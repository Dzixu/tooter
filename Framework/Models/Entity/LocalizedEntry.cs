﻿using System;

namespace Framework.Models.Entity
{
    public class LocalizedEntry : ILocalizedEntry
    {
        public string TranslationId { get; set; }
        public string LanguageCode { get; set; }
        public bool Deleted { get; set; }
        public DateTime DeletedOn { get; set; }


        public LocalizedEntry()
        {
            TranslationId = Entity.GenerateNewId();
        }
    }
}
