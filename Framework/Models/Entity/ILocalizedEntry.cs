﻿namespace Framework.Models.Entity
{
    public interface ILocalizedEntry : IArchiveEntity
    {
        string TranslationId { get; }
        string LanguageCode { get; set; }
    }
}
