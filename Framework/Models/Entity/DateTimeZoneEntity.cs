﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Framework.Models.Entity
{
    public class DateTimeZoneEntity
    {
        public DateTime DateTime { get; set; }
        public string TimeZoneId { get; set; }
    }
}
