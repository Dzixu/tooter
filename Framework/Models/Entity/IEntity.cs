﻿using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Framework.Models.Entity
{
    public interface IEntity : IArchiveEntity
    {
        [BsonId]
        string Id { get; set; }

        [BsonIgnore]
        ObjectId ObjectId { get; }

        [BsonIgnore]
        DateTime CreatedOn { get; }

        DateTime ModifiedOn { get; }

        string EntityType { get; }
    }
}
