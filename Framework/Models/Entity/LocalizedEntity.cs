﻿using MongoDB.Bson.Serialization.Attributes;
using System.Collections.Generic;

namespace Framework.Models.Entity
{
    [BsonIgnoreExtraElements(Inherited = true)]
    public class LocalizedEntity<T> : Entity where T: ILocalizedEntry
    {
        public ICollection<T> LocalizedItems { get; set; }
    }
}
