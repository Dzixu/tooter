﻿using System;
using Functional.Maybe;

namespace Framework.Extensions
{
    public static class MaybeExtensions
    {
        public static void OrElse<T>(this Maybe<T> maybe, Action @else)
        {
            maybe.Match(x => { }, @else);
        }
    }
}