﻿using Framework.Helpers;
using Functional.Maybe;
using System;
using System.Linq;

namespace Framework.Extensions
{
    public static class StringExtansions
    {
        public static string Capitalize(this string input)
        {
            if (String.IsNullOrEmpty(input))
            {
                return String.Empty;
            }
            return input.First().ToString().ToUpper() + String.Join("", input.Skip(1));
        }

        public static Maybe<DateTime> FromStringToDate(this string datetime)
        {
            return DateHelper.FromStringToDate(datetime);
        }
    }
}
