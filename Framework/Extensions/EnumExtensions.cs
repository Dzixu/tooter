﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.ComponentModel;

namespace Framework.Extensions
{
    public static class EnumExtensions
    {
        public static string ToDescription(this Enum en)
        {
            return GetDescription(en);
        }

        public static string GetDescription<T>(T element)
        {
            Type type = element.GetType();

            var items = element.ToString().Split(new[] { ", " }, StringSplitOptions.RemoveEmptyEntries);

            return string.Join(", ", items.Select(item => GetEnumDisplayName(type, item)));
        }

        public static IEnumerable<T> GetValues<T>()
        {
            return Enum.GetValues(typeof(T)).Cast<T>();
        }

        public static IEnumerable<string> GetDescriptions<T>()
        {
            return Enum.GetValues(typeof(T)).Cast<T>().Select(x => GetEnumDisplayName(typeof(T), x.ToString()));
        }

        public static IEnumerable<T> GetFlagValueCollection<T>(this Enum item)
        {
            return Enum.GetValues(item.GetType()).Cast<Enum>().Where(x => item.HasFlag(x)).Cast<T>();
        }

        private static string GetEnumDisplayName(Type type, string item)
        {
            string result = item.ToString();

            MemberInfo[] memInfo = type.GetMember(item);

            if (memInfo != null && memInfo.Length > 0)
            {
                object[] attrs = memInfo[0].GetCustomAttributes(typeof(DescriptionAttribute), false);

                if (attrs != null && attrs.Length > 0)
                {
                    var description = ((DescriptionAttribute)attrs[0]).Description;
                    result = description;
                }
            }

            return result;
        }
    }
}
