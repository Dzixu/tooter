﻿using System;

namespace Framework.Extensions
{
    public static class BoolExtensions
    {
        public static void Match(this bool input, Action onTrue, Action onFalse)
        {
            if (input)
            {
                onTrue();
            }
            else
            {
                onFalse();
            }
        }

        public static void Do(this bool input, Action onTrue)
        {
            if (input)
            {
                onTrue();
            }
        }

        public static void OrElse(this bool input, Action onFalse)
        {
            if (!input)
            {
                onFalse();
            }
        }
    }
}
