﻿using Functional.Maybe;
using System;

namespace Framework.Extensions
{
    public static class MaybeStringExtensions
    {
        public static Maybe<string> Do(this Maybe<string> m, Action<string> fn)
        {
            if (m != Maybe<string>.Nothing && !string.IsNullOrWhiteSpace(m.Value))
            {
                fn(m.Value);
            }

            return m;
        }

        public static Maybe<string> Do(this Maybe<string> m, Action fn)
        {
            if (m != Maybe<string>.Nothing && !string.IsNullOrWhiteSpace(m.Value))
            {
                fn();
            }

            return m;
        }

        public static Maybe<string> Match(this Maybe<string> m, Action<string> fn, Action @else)
        {
            if (m != Maybe<string>.Nothing && !string.IsNullOrWhiteSpace(m.Value))
            {
                fn(m.Value);
            }
            else
            {
                @else();
            }

            return m;
        }

        public static Maybe<string> Match(this Maybe<string> m, Action fn, Action @else)
        {
            if (m != Maybe<string>.Nothing && !string.IsNullOrWhiteSpace(m.Value))
            {
                fn();
            }
            else
            {
                @else();
            }

            return m;
        }
    }
}
