﻿using Framework.Helpers;
using System;

namespace Framework.Extensions
{
    public static class DateTimeExtensions
    {
        public static bool IsInRange(this DateTime dateToCheck, DateTime startDate, DateTime endDate)
        {
            return dateToCheck >= startDate && dateToCheck < endDate;
        }

        public static string FromDateToString(this DateTime dateTime)
        {
            return DateHelper.FromDateToString(dateTime);
        }
    }
}
