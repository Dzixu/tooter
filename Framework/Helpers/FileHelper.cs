﻿using System.IO;
using System.Collections.Generic;
using Framework.Dependency;

namespace Framework.Helpers
{
    public interface IFileHelper : IDependency
    {
        bool Exists(string fileName);
        bool IsOrganizerLogoValid(string fileName);
        bool IsCsvValid(string fileName);
    }

    public class FileHelper : IFileHelper
    {
        public bool Exists(string fileName)
        {
            return File.Exists(fileName);
        }

        public bool IsOrganizerLogoValid(string fileName)
        {
            bool result = false;

            ICollection<string> validExtensions = new[] { ".png", ".jpg", ".jpeg" };
            var fileExtension = Path.GetExtension(fileName).ToLower();

            if (validExtensions.Contains(fileExtension))
            {
                result = true;
            }

            return result;
        }

        public bool IsCsvValid(string fileName)
        {
            bool result = false;

            ICollection<string> validExtensions = new[] { ".csv"};
            var fileExtension = Path.GetExtension(fileName).ToLower();

            if (validExtensions.Contains(fileExtension))
            {
                result = true;
            }

            return result;
        }
    }
}
