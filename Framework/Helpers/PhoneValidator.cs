﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Framework.Helpers
{
    public class PhoneValidator
    {
        public static bool Validate(string number)
        {
            var result = false;
            if (number == null)
            {
                result = true;
            }
            if (number != null)
            {
                result = Regex.Match(number, @"^\+?\d{1,20}$").Success;
            }
            return result;
        }
    }
}
