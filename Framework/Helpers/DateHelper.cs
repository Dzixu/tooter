﻿using System;
using System.Globalization;
using Functional.Maybe;
using Framework.Extensions;

namespace Framework.Helpers
{
    public static class DateHelper
    {
        public const string dateTimeFormat = "dd-MM-yyyyTHH:mm:ss";

        public static Maybe<DateTime> FromStringToDate(string datetime)
        {
            var result = Maybe<DateTime>.Nothing;
            DateTime parsedDate;

            DateTime.TryParseExact
            (
                datetime, dateTimeFormat, new CultureInfo("pl-PL"),
                DateTimeStyles.None, out parsedDate
            ).Do(() => result = parsedDate.ToMaybe());

            return result;
        }

        public static string FromDateToString(DateTime dateTime)
        {
            return dateTime.ToString(dateTimeFormat);
        }
    }
}
