﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Resources;
using System.Text;
using System.Threading.Tasks;

namespace Framework.Helpers
{
    public static class EnumHelpers
    {
        public static string GetDescription(object objEnum)
        {
            var fi = objEnum.GetType().GetField(objEnum.ToString());
            var attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);

            string result = string.Empty;

            if (attributes.Length > 0)
            {
                result = attributes[0].Description;
            }
            else
            {
                result = objEnum.ToString();
            }

            return result;
        }

        public static string DescriptionContent(this Enum value)
        {
            var fi = value.GetType().GetField(value.ToString());
            var attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);

            string result = string.Empty;
            if (!string.IsNullOrWhiteSpace(attributes[0].Description))
            {
                result = attributes[0].Description;
            }

            return result;
        }

        public static T GetValueFromDescription<T>(string description)
        {
            var type = typeof(T);
            if (!type.IsEnum) throw new InvalidOperationException();
            if (!string.IsNullOrWhiteSpace(description))
            {
                foreach (var field in type.GetFields())
                {
                    var attribute = Attribute.GetCustomAttribute(field,
                        typeof(DescriptionAttribute)) as DescriptionAttribute;
                    if (attribute != null)
                    {
                        if (attribute.Description.ToLower() == description.ToLower())
                            return (T)field.GetValue(null);
                    }
                    else
                    {
                        if (field.Name.ToLower() == description.ToLower())
                            return (T)field.GetValue(null);
                    }
                }
            }

            return default(T);
        }
    }
}

