﻿using MongoDB.Bson;
using System.Text.RegularExpressions;

namespace Framework.Helpers
{
    public static class ObjectIdHelper
    {
        public static string GenerateNewObjectId()
        {
            return ObjectId.GenerateNewId().ToString();
        }

        public static bool ValidObjectId(string objectId)
        {
            return Regex.IsMatch(objectId, @"\A\b[0-9a-fA-F]+\b\Z");
        }
    }
}
