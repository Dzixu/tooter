﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Framework.Services
{
    public class ServiceResult<TModel> : ServiceResult
    {
        public TModel Model { get; set; }

        public ServiceResult(TModel model = default(TModel))
        {
            Model = model;
        }

        public ServiceResult(ServiceResult serviceResult)
        {
            AddErrors(serviceResult.ValidationResults);
        }

        public void Do(Action<ServiceResult<TModel>> action)
        {
            if (IsValid)
            {
                action(this);
            }
        }

        public void Match(Action<ServiceResult<TModel>> valid, Action<ServiceResult<TModel>> invalid)
        {
            if (IsValid)
            {
                valid(this);
            }
            else
            {
                invalid(this);
            }
        }
    }
}
