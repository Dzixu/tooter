﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Framework.Services
{
    public interface IFileUtilsService
    {
        bool IsOfType(string fileName, string extension);
        bool IsOfType(string fileName, params string[] extensions);
    }

    public class FileUtilsService : IFileUtilsService
    {
        public bool IsOfType(string fileName, string extension)
        {
            var fileExtension = Path.GetExtension(fileName).Replace(".", "");
            return fileExtension == extension;
        }

        public bool IsOfType(string fileName, params string[] extensions)
        {
            return extensions.Any(x => IsOfType(fileName, x));
        }
    }
}
