﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Framework.Services
{
    public class IdentifierResult : ServiceResult
    {
        public string FileId { get; set; }
    }
}
