﻿using FluentValidation.Results;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Framework.Services
{
    public class ServiceResult
    {
        public class ValidationResult
        {
            public string ErrorMessage { get; set; }
            public string PropertyName { get; set; }

            public ValidationResult()
            {
            }

            public ValidationResult(string errorMessage, string propertyName)
            {
                ErrorMessage = errorMessage;
                PropertyName = propertyName;
            }

            public ValidationResult(string errorMessage)
                : this(errorMessage, "")
            {
            }
        }

        private List<ValidationResult> _validationResults = new List<ValidationResult>();

        public IEnumerable<ValidationResult> ValidationResults
        {
            get
            {
                return _validationResults;
            }
        }

        public bool IsValid
        {
            get
            {
                return _validationResults.Any() == false;
            }
        }

        public void AddError(ValidationResult validationResult)
        {
            _validationResults.Add(validationResult);
        }

        public void AddError(string errorMessage, string propertyName)
        {
            AddError(new ValidationResult(errorMessage, propertyName));
        }

        public void AddError(string errorMessage)
        {
            AddError(new ValidationResult(errorMessage));
        }

        public void AddErrors(IEnumerable<ValidationResult> validationResults)
        {
            _validationResults.AddRange(validationResults);
        }

        public void AddErrors(ServiceResult validationResults)
        {
            _validationResults.AddRange(validationResults.ValidationResults);
        }

        public void AddErrors(IList<ValidationFailure> validationFailures)
        {
            _validationResults.AddRange(validationFailures.Select(x => new ValidationResult(x.ErrorMessage, x.PropertyName)));
        }

        public void Do(Action<ServiceResult> action)
        {
            if (IsValid)
            {
                action(this);
            }
        }

        public void Do(Action action)
        {
            if (IsValid)
            {
                action();
            }
        }

        public void Match(Action<ServiceResult> valid, Action<ServiceResult> invalid)
        {
            if (IsValid)
            {
                valid(this);
            }
            else
            {
                invalid(this);
            }
        }

        public void OrElse(Action<ServiceResult> invalid)
        {
            if (!IsValid)
            {
                invalid(this);
            }
        }

        public override string ToString()
        {
            if (!ValidationResults.Any())
            {
                return string.Empty;
            }

            if (ValidationResults.Count() > 1)
            {
                var sb = new StringBuilder();
                ValidationResults.ToList().ForEach(x => sb.AppendLine($"- {x.ErrorMessage}"));
                return sb.ToString();
            }
            else
            {
                return ValidationResults.First().ErrorMessage;
            }
        }
    }
}
