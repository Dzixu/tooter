﻿using Framework.Models.FileOptions;

namespace Framework.Services
{
    public class FileResult : ServiceResult
    {
        public FileBase FileBase { get; set; }
        
    }
}
